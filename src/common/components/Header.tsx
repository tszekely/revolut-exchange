import styled from 'styled-components';

const StyledHeader = styled.header`
  margin-bottom: 1rem;

  h1 {
    margin: 0;
    font-size: 2rem;
    line-height: 1;
  }
`;

const Header: React.FC = ({ children }) => {
  return (
    <StyledHeader>
      <h1>{children}</h1>
    </StyledHeader>
  );
};

export default Header;
