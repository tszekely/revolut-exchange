import styled from 'styled-components';

const Button = styled.button`
  appearance: none;
  border-radius: 0.5rem;
  border: none;
  font: bold 1rem / 2 var(--font-family);
  color: #ffffff;
  padding: 0.5rem;
  background-color: var(--alt-color);
  transition: background-color 100ms;

  &:hover,
  &:active {
    background-color: var(--alt-color-darker);
  }
`;

export default Button;
