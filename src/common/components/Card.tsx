import styled from 'styled-components';

const Card = styled.article`
  background-color: #ffffff;
  margin-bottom: 1rem;
  padding: 0.5rem 1rem;
  border-radius: 0.5rem;
`;

export default Card;
