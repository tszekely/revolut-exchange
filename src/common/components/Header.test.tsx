import { render, screen } from '@testing-library/react';
import Header from './Header';

test('renders correctly', () => {
  render(<Header>Header content</Header>);
  const headerElement = screen.getByText('Header content');
  expect(headerElement).toBeInTheDocument();
});
