import {
  formatCurrency,
  formatCurrencyPrecise,
  formatMoneyAmount,
} from './utils';

it('formatCurrency should format correctly', () => {
  expect(
    formatCurrency({
      currency: 'USD',
      amount: 10000,
    })
  ).toBe('$10,000.00');
});

it('formatCurrency should round values with more than 2 decimals', () => {
  expect(
    formatCurrency({
      currency: 'EUR',
      amount: 2000.35641,
    })
  ).toBe('€2,000.36');
});

it('formatCurrencyPrecise should display all decimals', () => {
  expect(
    formatCurrencyPrecise({
      currency: 'GBP',
      amount: 3000.4678563,
    })
  ).toBe('£3,000.4678563');
});

it('formatMoneyAmount should format numeric values to maximum 2 decimals', () => {
  expect(formatMoneyAmount(150)).toBe('150');
  expect(formatMoneyAmount(1.5)).toBe('1.5');
  expect(formatMoneyAmount(1.555)).toBe('1.56');
  expect(formatMoneyAmount(0.01234)).toBe('0.01');
});
