export const formatCurrency = ({ currency, amount }: MonetaryValue) =>
  Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: currency,
  }).format(amount);

export const formatCurrencyPrecise = ({ currency, amount }: MonetaryValue) =>
  Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: currency,
    maximumFractionDigits: 20,
  }).format(amount);

export const formatMoneyAmount = (amount: number) =>
  Intl.NumberFormat('en-US', {
    maximumFractionDigits: 2,
    useGrouping: false,
  }).format(amount);
