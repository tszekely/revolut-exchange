declare type MonetaryValue = {
  currency: string;
  amount: number;
};

declare type MoneyAccount = {
  currency: string;
  amount: number;
};
