import styled from 'styled-components';
import { useAppSelector } from '../common/hooks';
import { selectHasExchangeState } from '../features/account/accountSlice';
import AccountsPage from '../features/account/AccountsPage';
import { ExchangePage } from '../features/exchange/ExchangePage';

const StyledApp = styled.div`
  padding: 1rem;
  height: 100%;
  max-width: 420px;
  margin: auto;
  flex: 1 1 auto;
  overflow-y: auto;
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
    height: auto;
    min-height: 420px;
    background-color: var(--bg-color);
    border-radius: 0.5rem;
  }
`;

const App: React.FC = () => {
  const hasExchangeState = useAppSelector(selectHasExchangeState);

  return (
    <StyledApp role="application">
      {hasExchangeState ? <ExchangePage /> : <AccountsPage />}
    </StyledApp>
  );
};

export default App;
