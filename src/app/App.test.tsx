import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../app/store';
import App from './App';

beforeEach(() => {
  render(
    <Provider store={store}>
      <App />
    </Provider>
  );
});

it('should render the Accounts list', () => {
  const headerElement = screen.getByText('Accounts');
  expect(headerElement).toBeInTheDocument();

  const accountCards = screen.getAllByRole('listitem');
  expect(accountCards.length).toBe(4);
});

it('should switch to the Exchange page when an exchange button is clicked', async () => {
  const exchangeButton = screen.getAllByRole('button')[0];

  userEvent.click(exchangeButton);

  const loaderElement = screen.getByRole('progressbar');
  expect(loaderElement).toBeInTheDocument();

  await waitFor(() => {
    const headerElement = screen.getByText('Sell USD');
    expect(headerElement).toBeInTheDocument();
  });
});
