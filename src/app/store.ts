import { configureStore } from '@reduxjs/toolkit';
import { default as accountReducer } from '../features/account/accountSlice';
import { default as exchangeReducer } from '../features/exchange/exchangeSlice';

export const store = configureStore({
  reducer: {
    account: accountReducer,
    exchange: exchangeReducer,
  },
  devTools: true,
  preloadedState: {},
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
