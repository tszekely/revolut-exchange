import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { StyleSheetManager } from 'styled-components';
import App from './app/App';
import { store } from './app/store';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <StyleSheetManager disableVendorPrefixes>
      <Provider store={store}>
        <App />
      </Provider>
    </StyleSheetManager>
  </React.StrictMode>,
  document.getElementById('root')
);
