import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  border: none;
  background-color: transparent;
  font-size: 1rem;
  font-weight: bold;
  text-align: right;
  padding: 0 0.25rem;
  height: 2rem;
  width: calc(100% - 3.875rem - 1rem);
`;

const validateNumberFormat: (value: string) => boolean = (value) =>
  /^[1-9]{1}\d*(\.\d{0,2})?$/.test(value);

const ValidatedNumberInput: React.FC<{
  onChange: (amount: string) => void;
  value: string;
}> = ({ onChange, value }) => {
  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const newValue = e.currentTarget.value;

    if (
      (validateNumberFormat(newValue) && parseFloat(newValue) > 0) ||
      newValue === ''
    ) {
      onChange(newValue);
    }
  };

  return (
    <Input type="text" placeholder="0" onChange={handleChange} value={value} />
  );
};

export default ValidatedNumberInput;
