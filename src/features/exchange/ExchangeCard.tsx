import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import styled from 'styled-components';
import Card from '../../common/components/Card';
import { useAppDispatch, useAppSelector } from '../../common/hooks';
import { formatCurrency } from '../../common/utils';
import { selectAccounts } from '../account/accountSlice';
import ValidatedNumberInput from './ValidatedNumberInput';


const StyledCard = styled(Card)`
  display: flex;
  flex-wrap: wrap;
  margin: 0;
`;

const Select = styled.select`
  border: none;
  background-color: transparent;
  font-size: 1rem;
  font-weight: bold;
  padding: 0;
  height: 2rem;
  width: 3.875rem;
  margin-right: 1rem;
`;

const Balance = styled.div`
  padding: 0 0.2rem 0.5rem;
  font-size: 0.9rem;
  color: #757575;
`;

const Error = styled.div`
  padding: 0 0.25rem 0.5rem;
  font-size: 0.8rem;
  color: #d32f2f;
  align-self: flex-end;
  margin-left: auto;
`;

export const ExchangeCard: React.FC<{
  account: MoneyAccount;
  onChangeCurrency: ActionCreatorWithPayload<string, string>;
  onChangeAmount: (amount: string) => void;
  amount: string;
  isValid: boolean;
}> = ({ account, onChangeCurrency, onChangeAmount, amount, isValid }) => {
  const accounts = useAppSelector(selectAccounts);
  const appDispatch = useAppDispatch();

  return (
    <StyledCard>
      <Select
        value={account?.currency}
        onChange={({ currentTarget: { value } }) =>
          appDispatch(onChangeCurrency(value))
        }
      >
        {accounts.map(({ currency }) => (
          <option value={currency} key={currency}>
            {currency}
          </option>
        ))}
      </Select>

      <ValidatedNumberInput onChange={onChangeAmount} value={amount} />

      <Balance>Balance: {formatCurrency(account)}</Balance>

      {!isValid && parseFloat(amount) > 0 && <Error>Exceed balance</Error>}
    </StyledCard>
  );
};
