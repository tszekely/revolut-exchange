import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

const apiKey = process.env.REACT_APP_OPENEXCHANGE_API_KEY;

export const fetchExchangeRates = createAsyncThunk<{
  rates: Record<string, number>;
}>('exchange/fetchExchangeRates', async () => {
  const response = await fetch(
    `https://openexchangerates.org/api/latest.json?app_id=${apiKey}`
  );
  return await response.json();
});

export interface ExchangeState {
  rates: Record<string, number>;
}

const initialState: ExchangeState = {
  rates: {},
};

const exchangeSlice = createSlice({
  name: 'exchange',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchExchangeRates.fulfilled, (state, action) => {
      state.rates = action.payload.rates;
    });
  },
});

// Ideally this should be recalculated when the main currency changes through an API request,
// but due to the limitations of the Free OpenExchange tier, it's calculated locally
export const selectExchangeRate =
  (fromCurrency: string, toCurrency: string) => (state: RootState) =>
    state.exchange.rates[toCurrency] / state.exchange.rates[fromCurrency];

export const selectExchangeRatesHaveLoaded = (state: RootState) =>
  Object.keys(state.exchange.rates).length > 0;

export default exchangeSlice.reducer;
