import { useEffect, useState } from 'react';
import styled from 'styled-components';
import Button from '../../common/components/Button';
import Header from '../../common/components/Header';
import DownArrowIcon from '../../common/components/icons/DownArrowIcon';
import StonksIcon from '../../common/components/icons/StonksIcon';
import Loader from '../../common/components/Loader';
import { useAppDispatch, useAppSelector } from '../../common/hooks';
import { formatCurrencyPrecise, formatMoneyAmount } from '../../common/utils';
import { ExchangeCard } from './ExchangeCard';

import {
  fetchExchangeRates,
  selectExchangeRate,
  selectExchangeRatesHaveLoaded,
} from './exchangeSlice';
import {
  clearCurrencies,
  doExchange,
  selectBottomAccount,
  selectTopAccount,
  setDestinationCurrency,
  setSourceCurrency,
} from '../../features/account/accountSlice';

const SwitchButton = styled.button`
  appearance: none;
  padding: 0.5rem;
  border: 0.35rem solid var(--bg-color);
  border-radius: 50%;
  margin: -0.75rem auto;
  display: block;
  position: relative;
  z-index: 1;
  background: #ffffff;

  .icon {
    width: 1rem;
    display: block;
    fill: var(--primary-color);
    transition: transform 100ms;
  }

  &.isBuy {
    .icon {
      transform: rotate(-180deg);
    }
  }
`;

const BackIcon = styled(DownArrowIcon)`
  transform: rotate(90deg);
  width: 1rem;
  padding: 0.5rem;
  margin-right: 0.5rem;
  box-sizing: content-box;
  vertical-align: middle;
`;

const ExchangeRate = styled.div`
  color: var(--primary-color);
  font-size: 0.875rem;
  line-height: 2;
  position: relative;
  bottom: 0.75rem;

  .icon {
    fill: var(--primary-color);
    width: 0.875rem;
    margin-right: 0.25rem;
    display: inline-block;
    vertical-align: middle;
  }
`;

const ExchangeButton = styled(Button)`
  margin: auto 0 0;
  background-color: var(--primary-color);

  &:disabled {
    background-color: var(--alt-color);
    pointer-events: none;
  }
`;

export const ExchangePage = () => {
  const topAccount = useAppSelector(selectTopAccount);
  const bottomAccount = useAppSelector(selectBottomAccount);
  const exchangeRate = useAppSelector(
    selectExchangeRate(topAccount.currency, bottomAccount.currency)
  );
  const exchangeRatesHaveLoaded = useAppSelector(selectExchangeRatesHaveLoaded);

  const appDispatch = useAppDispatch();

  const [isSellExchange, setIsSellExchange] = useState(true);
  const [topAmount, setTopAmount] = useState('');
  const [bottomAmount, setBottomAmount] = useState('');

  const topAmountNumber = parseFloat(topAmount);
  const bottomAmountNumber = parseFloat(bottomAmount);

  useEffect(() => {
    appDispatch(fetchExchangeRates());

    const interval = setInterval(
      () => appDispatch(fetchExchangeRates()),
      10000
    );

    return () => {
      clearInterval(interval);
    };
  }, [appDispatch]);

  const handleChangeTopAmount = (value: string) => {
    setTopAmount(value);
    setBottomAmount(
      value === '' ? value : formatMoneyAmount(parseFloat(value) * exchangeRate)
    );
  };

  const handleChangeBottomAmount = (value: string) => {
    setBottomAmount(value);
    setTopAmount(
      value === '' ? value : formatMoneyAmount(parseFloat(value) / exchangeRate)
    );
  };

  const handleExchange = () => {
    appDispatch(
      doExchange(
        isSellExchange
          ? {
              fromMonetaryValue: {
                currency: topAccount.currency,
                amount: topAmountNumber,
              },
              toMonetaryValue: {
                currency: bottomAccount.currency,
                amount: bottomAmountNumber,
              },
            }
          : {
              fromMonetaryValue: {
                currency: bottomAccount.currency,
                amount: bottomAmountNumber,
              },
              toMonetaryValue: {
                currency: topAccount.currency,
                amount: topAmountNumber,
              },
            }
      )
    );

    setTopAmount('');
    setBottomAmount('');
  };

  const isTopAmountValid = isSellExchange
    ? topAmountNumber > 0 && topAmountNumber <= topAccount.amount
    : true;
  const isBottomAmountValid = !isSellExchange
    ? bottomAmountNumber > 0 && bottomAmountNumber <= bottomAccount.amount
    : true;

  return exchangeRatesHaveLoaded ? (
    <>
      <Header>
        <BackIcon onClick={() => appDispatch(clearCurrencies())} />
        <span>
          {isSellExchange
            ? `Sell ${topAccount.currency}`
            : `Buy ${topAccount.currency}`}
        </span>
      </Header>

      <ExchangeRate role="status" aria-label="Exchange rate">
        <StonksIcon className="icon" />
        <span>{`${formatCurrencyPrecise({
          currency: topAccount.currency,
          amount: 1,
        })} = ${formatCurrencyPrecise({
          currency: bottomAccount.currency,
          amount: exchangeRate,
        })}`}</span>
      </ExchangeRate>

      <ExchangeCard
        account={topAccount}
        onChangeCurrency={setSourceCurrency}
        onChangeAmount={handleChangeTopAmount}
        amount={topAmount}
        isValid={isTopAmountValid}
      />

      <SwitchButton
        onClick={() => setIsSellExchange(!isSellExchange)}
        className={isSellExchange ? '' : 'isBuy'}
      >
        <DownArrowIcon className="icon" />
      </SwitchButton>

      <ExchangeCard
        account={bottomAccount}
        onChangeCurrency={setDestinationCurrency}
        onChangeAmount={handleChangeBottomAmount}
        amount={bottomAmount}
        isValid={isBottomAmountValid}
      />

      <ExchangeButton
        disabled={!(isTopAmountValid && isBottomAmountValid)}
        onClick={handleExchange}
      >
        {isSellExchange
          ? `Sell ${topAccount.currency} to ${bottomAccount.currency}`
          : `Buy ${topAccount.currency} with ${bottomAccount.currency}`}
      </ExchangeButton>
    </>
  ) : (
    <Loader />
  );
};
