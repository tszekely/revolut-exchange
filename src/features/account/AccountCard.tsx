import { HTMLAttributes } from 'react';
import styled from 'styled-components';
import Button from '../../common/components/Button';
import Card from '../../common/components/Card';
import ExchangeIcon from '../../common/components/icons/ExchangeIcon';
import { useAppDispatch } from '../../common/hooks';
import { formatCurrency } from '../../common/utils';
import { setSourceCurrency } from './accountSlice';

const StyledCard = styled(Card)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Title = styled.div`
  font-size: 1.5rem;
  font-weight: bold;
`;

const ExchangeButton = styled(Button)`
  .icon {
    fill: var(--primary-color);
    width: 1.5rem;
    display: block;
  }
`;

const AccountCard: React.FC<
  HTMLAttributes<HTMLElement> & { account: MoneyAccount }
> = ({ account, ...props }) => {
  const appDispatch = useAppDispatch();

  return (
    <StyledCard {...props}>
      <Title>{formatCurrency(account)}</Title>
      <ExchangeButton
        onClick={() => appDispatch(setSourceCurrency(account.currency))}
        title="Exchange"
      >
        <ExchangeIcon className="icon" />
      </ExchangeButton>
    </StyledCard>
  );
};

export default AccountCard;
