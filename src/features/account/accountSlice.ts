import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import type { RootState } from '../../app/store';

interface AccountState {
  accounts: MoneyAccount[];
  topAccountCurrency?: string;
  bottomAccountCurrency?: string;
}

const initialState: AccountState = {
  accounts: [
    {
      currency: 'USD',
      amount: 5000,
    },
    {
      currency: 'EUR',
      amount: 10000,
    },
    {
      currency: 'GBP',
      amount: 1500,
    },
    {
      currency: 'RON',
      amount: 38000,
    },
  ],
  topAccountCurrency: undefined,
  bottomAccountCurrency: undefined,
};

export const accountSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    setSourceCurrency: (state, action: PayloadAction<string>) => {
      state.topAccountCurrency = action.payload;

      if (
        !state.bottomAccountCurrency ||
        state.topAccountCurrency === state.bottomAccountCurrency
      ) {
        // set the default destination account currency to the first non-identical account'
        const bottomAccount = state.accounts.find(
          (account) => account.currency !== state.topAccountCurrency
        );

        state.bottomAccountCurrency = bottomAccount?.currency;
      }
    },
    setDestinationCurrency: (state, action: PayloadAction<string>) => {
      state.bottomAccountCurrency = action.payload;

      if (
        !state.topAccountCurrency ||
        state.topAccountCurrency === state.bottomAccountCurrency
      ) {
        // set the default source account currency to the first non-identical account'
        const bottomAccount = state.accounts.find(
          (account) => account.currency !== state.bottomAccountCurrency
        );

        state.topAccountCurrency = bottomAccount?.currency;
      }
    },
    clearCurrencies: (state) => {
      state.topAccountCurrency = undefined;
      state.bottomAccountCurrency = undefined;
    },
    doExchange: (
      state,
      action: PayloadAction<{
        fromMonetaryValue: MonetaryValue;
        toMonetaryValue: MonetaryValue;
      }>
    ) => {
      const { fromMonetaryValue, toMonetaryValue } = action.payload;

      state.accounts.forEach((account) => {
        if (account.currency === fromMonetaryValue.currency) {
          account.amount -= fromMonetaryValue.amount;
        }

        if (account.currency === toMonetaryValue.currency) {
          account.amount += toMonetaryValue.amount;
        }
      });
    },
  },
});

export const {
  setSourceCurrency,
  setDestinationCurrency,
  clearCurrencies,
  doExchange,
} = accountSlice.actions;

export const selectAccounts = (state: RootState) => state.account.accounts;

export const selectHasExchangeState = (state: RootState) =>
  Boolean(
    state.account.topAccountCurrency && state.account.bottomAccountCurrency
  );

export const selectTopAccount = (state: RootState) =>
  state.account.accounts.find(
    (account) => account.currency === state.account.topAccountCurrency
  ) as MoneyAccount;

export const selectBottomAccount = (state: RootState) =>
  state.account.accounts.find(
    (account) => account.currency === state.account.bottomAccountCurrency
  ) as MoneyAccount;

export default accountSlice.reducer;
