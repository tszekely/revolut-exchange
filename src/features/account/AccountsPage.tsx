import Header from '../../common/components/Header';
import { useAppSelector } from '../../common/hooks';
import AccountCard from './AccountCard';
import { selectAccounts } from './accountSlice';

const AccountsPage = () => {
  const accounts = useAppSelector(selectAccounts);

  return (
    <>
      <Header>Accounts</Header>
      <div role="list">
        {accounts.map((account) => (
          <AccountCard
            key={account.currency}
            account={account}
            role="listitem"
          />
        ))}
      </div>
    </>
  );
};

export default AccountsPage;
